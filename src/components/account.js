import React, { Component } from 'react';
import {
  Navbar,
  Nav,
  NavItem,
  Jumbotron,
} from 'reactstrap';

class Account extends Component {
  render() {
    return (
      <div className="account">
      <Navbar color="light" light expand="md">
          <a href="/" className="Brand">EVISION ASSIGNMENT</a>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <a href="https://github.com/reactstrap/reactstrap">bitbucket repo</a>
              </NavItem>
            </Nav>
        </Navbar>
        <Jumbotron>
	        <h2 className="display-3">Hello, {this.props.account.name}!</h2>
	        <p className="lead">IBAN: {this.props.account.iban}</p>
	        <h3>Balance: <b>{ this.props.account.balance }</b></h3>
	    </Jumbotron>
      </div>
    );
  }
}

export default Account;
