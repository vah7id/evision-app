import React, { Component } from 'react';
import { Table, Button } from 'reactstrap';

class CardList extends Component {
  render() {
    return (
      <div className="cardlist">
        <Table bordered hover>
        	<thead>
        		<tr>
	        		<th>Interaction</th>
	        		<th>Description</th>
	        		<th>Date</th>
	        		<th>Amount</th>
        		</tr>
        	</thead>
        	<tbody>
        		{
        			this.props.cardList &&
        			this.props.cardList.map((card)=>{
        				return (<tr>
        					<td>{ card.from ? 'FROM '+card.from : 'TO '+card.to }</td>
        					<td>{ card.description }</td>
        					<td>{ card.date }</td>
        					<td>{ card.amount }</td>
        				</tr>)
        			})
        		}
        	</tbody>
        </Table>
      </div>
    );
  }
}

export default CardList;
