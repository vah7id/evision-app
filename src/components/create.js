import React, { Component } from 'react';
import { Button, Alert, Modal, CustomInput, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

class Create extends Component {
  constructor(props){
  	super(props);
    this.state = {
      modal: false,
      alert: false,
      from: false,
      to: false,
      name: '',
      amount: 0,
      description: '',
      date: new Date(),
    };
    this.toggle = this.toggle.bind(this);
    this.onDismiss = this.onDismiss.bind(this);
  }
  toggle() {
    this.setState({ modal: !this.state.modal });
  }
  onChange(e,key) {
  	this.state[key] = e.target.value;
  	this.setState(this.state);
  }
  onDismiss() {
    this.setState({ alert: false });
  }
  submit(){
  	this.props.addCard(this.state);
  	this.toggle();
  	this.setState({ alert: true });
  }
  render() {
    return (
      <div className="Create">
        <Button color="primary" onClick={this.toggle}>ADD NEW DEBIT CARD</Button>
        <Alert color="success" isOpen={this.state.alert} toggle={this.onDismiss}>Card added successfully!</Alert>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Add Card</ModalHeader>
          <ModalBody>
          	<Form>
          		<FormGroup>
		          <div>
		            <CustomInput onChange={()=>this.setState({ from: true, to: false })} value={this.state.from} type="radio" value={this.state.from} id="exampleCustomRadio" name="customRadio" label="From somebody" />
		            <CustomInput onChange={()=>this.setState({ to: true, from: false })} value={this.state.to} type="radio" value={this.state.to} id="exampleCustomRadio2" name="customRadio" label="To somebody" />
		          </div>
		        </FormGroup>
		        <FormGroup>
		          	<Input required onChange={(e)=>this.onChange(e,'name')} value={this.state.name} type="text" name="person" placeholder="Person name" />
		        </FormGroup>
          		<FormGroup>
		          <Label>Amount</Label>
		          <Input required onChange={(e)=>this.onChange(e,'amount')} value={this.state.amount} type="number" name="amount" placeholder="add an amount" />
		        </FormGroup>
		        <FormGroup>
		          <Label>Description</Label>
		          <Input required type="text" onChange={(e)=>this.onChange(e,'description')} value={this.state.description} name="description" placeholder="add a description" />
		        </FormGroup>
		        <FormGroup>
		          <Label>Date</Label>
		          <Input required type="date" onChange={(e)=>this.onChange(e,'date')} value={this.state.date} name="date" id="exampleDate" placeholder="date" />
		        </FormGroup>
		    </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={()=>this.submit()}>Create</Button>{' '}
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default Create;
