import React, { Component } from 'react';
import Account from './components/account';
import Create from './components/create';
import CardList from './components/cardList';
import { host, port } from './configs.dev';
import axios from 'axios';
import { Alert } from 'reactstrap';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      account: null,
      balance: null,
      currency: null,
      cardList: null,
    }
    this.addCard = this.addCard.bind(this);
  }
  componentWillMount() {
    this.fetchAccount();
  }
  fetchAccount() {
    const self = this;
    axios.get(host+':'+port+'/api/balance')
    .then(function (response) {
      self.setState({ 
        account: response.data.account,
        balance: response.data.account.balance,
        currency: response.data.currency,
        cardList: response.data.debitsAndCredits,
      });
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  addCard(data) {
    const self = this;
    axios.put(host+':'+port+'/api/balance/add',{
      amount: parseInt(data.amount),
      description: data.description,
      date: data.date,
      from: data.from && data.name,
      to: data.to && data.name
    })
    .then(function (response) {
      self.fetchAccount();
    })
    .catch(function (error) {
      alert(error.toString());
    });
  }
  render() {
    return (
      <div className="Wrapper">
        { this.state.account ? 
          <div className="App">
            <Account account={this.state.account} />
            <Create addCard={this.addCard} />
            <CardList cardList={this.state.cardList} />
          </div> :
          <p>Loading...</p>
        }
      </div>
    );
  }
}

export default App;
